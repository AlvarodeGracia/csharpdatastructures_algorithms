﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpDataStructures_Algorithms._03_Pilas_Colas
{
    class Pilas
    {

        public void Pilas_Example()
        {
            Stack<char> chars = new Stack<char>();
            foreach (char c in "LET'S REVERSE!")
            {
                chars.Push(c);
            }

            while (chars.Count > 0)
            {
                Console.Write(chars.Pop());
            }
            Console.WriteLine();
        }

        private const int DISCS_COUNT = 10;
        private const int DELAY_MS = 250;
        private static int _columnSize = 30;

        
    }
}
