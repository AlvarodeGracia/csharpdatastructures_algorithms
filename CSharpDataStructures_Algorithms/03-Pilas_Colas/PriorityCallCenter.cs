﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
//Instalar Nugget OptimizedPriorityQueue 
using Priority_Queue;

namespace CSharpDataStructures_Algorithms._03_Pilas_Colas
{
    class PriorityCallCenter
    {
        
        private int _counter = 0;
        public SimplePriorityQueue<IncomingCall> Calls { get; private set; }

        public PriorityCallCenter()
        {
            Calls = new SimplePriorityQueue<IncomingCall>();
        }

        public void Call(int clientId, bool isPriority = false)
        {
            IncomingCall call = new IncomingCall()
            {
                Id = ++_counter,
                ClientId = clientId,
                CallTime = DateTime.Now,
                IsPriority = isPriority
            };
            Calls.Enqueue(call, isPriority ? 0 : 1);
        }

        public IncomingCall Answer(string consultant)
        {
            if (Calls.Count > 0)
            {
                IncomingCall call = Calls.Dequeue();
                call.Consultant = consultant;
                call.StartTime = DateTime.Now;
                return call;
            }
            return null;
        }

        public void End(IncomingCall call)
        {
            call.EndTime = DateTime.Now;
        }

        public bool AreWaitingCalls()
        {
            return Calls.Count > 0;
        }

        static void Ejecutar(string[] args)
        {
            Random random = new Random();

            PriorityCallCenter center = new PriorityCallCenter();
            center.Call(1234);
            center.Call(5678, true);
            center.Call(1468);
            center.Call(9641, true);

            while (center.AreWaitingCalls())
            {
                IncomingCall call = center.Answer("Marcin");
                Log($"Call #{call.Id} from {call.ClientId}  is answered by { call.Consultant} / Mode: { (call.IsPriority ? "priority" : "normal")}."); 
                Thread.Sleep(random.Next(1000, 10000));
                center.End(call);
                Log($"Call #{call.Id} from {call.ClientId}       is ended by { call.Consultant}."); 
            }
        }
        private static void Log(string text)
        {
            Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}]  { text} "); 
        }

    }
}
