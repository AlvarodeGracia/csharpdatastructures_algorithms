﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpDataStructures_Algorithms._04_Dictionaries_Sets
{

    //NO SE RECOMIENDA USAR MEJOR DICTIONARY
    class Hash_Table
    {
        public void trabajar()
        {
            //Instancia de tablaHash
            Hashtable hashtable = new Hashtable();

            //Insertamos un valor con la clave key
            hashtable["key"] = "value";

            //Recogemos el valor de la clave Key
            string value = (string)hashtable["key"];
            
            //Recorremos la coleccion 
            foreach (DictionaryEntry entry in hashtable)
            {
                Console.WriteLine($"{entry.Key} - {entry.Value}");
            }
        }


        public void directorioTelefónico()
        {
            Hashtable phoneBook = new Hashtable()
            {
                { "Marcin Jamro", "000-000-000" },
                { "John Smith", "111-111-111" }
            };

            //AGREGAR
            phoneBook["Lily Smith"] = "333-333-333";

            try
            {
                phoneBook.Add("Mary Fox", "222-222-222");

                //Cuando utiliza el indexador para establecer un valor para una clave en particular, 
                //no arrojará ninguna excepción cuando ya haya un elemento con la clave dada. 
                //En tal situación, se actualizará un valor de este elemento.
            }
            catch (ArgumentException)
            {
                Console.WriteLine("The entry already exists.");
            }

            //RECORRER

            Console.WriteLine("Phone numbers:");
            if (phoneBook.Count == 0)
            {
                Console.WriteLine("Empty");
            }
            else
            {
                foreach (DictionaryEntry entry in phoneBook)
                {
                    Console.WriteLine($" - {entry.Key}: {entry.Value}");
                }
            }

            //BUSQUEDA

            Console.WriteLine();
            Console.Write("Search by name: ");
            string name = Console.ReadLine();
            if (phoneBook.Contains(name))
            {
                string number = (string)phoneBook[name];
                Console.WriteLine($"Found phone number: {number}");
            }
            else
            {
                Console.WriteLine("The entry does not exist.");
            }
        }
    }
}
