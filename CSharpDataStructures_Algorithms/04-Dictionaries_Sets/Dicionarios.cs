﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpDataStructures_Algorithms._04_Dictionaries_Sets
{
    class Dicionarios
    {


        public void teoria()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>
            {
                { "Key1", "Value1" },
                { "Key2", "Value2" }
            };

            string value = dictionary["key1"];

            //Cuidado puede no existir

            if(dictionary.ContainsKey("key1"))
                value = dictionary["key1"];

            try
            {
                value = dictionary["key1"];
            }
            catch(KeyNotFoundException ex)
            {

            }

            bool haveValue = dictionary.TryGetValue("key1", out value);


            //Comprobar si existe el cpontenido esta operacion le cuesta mas trabajo
            bool haveValue_2 = dictionary.ContainsValue("Value1");


            //Recorrer un array
            foreach (KeyValuePair<string, string> pair in dictionary)
            {
                Console.WriteLine($"{pair.Key} - {pair.Value}");
            }

            //TryAdd, TryUpdate, AddOrUpdate, y GetOrAdd

        }


        public void ubicacion_producto()
        {
            Dictionary<string, string> products = new Dictionary<string, string>
            {
                { "5900000000000", "A1" },
                { "5901111111111", "B5" },
                { "5902222222222", "C9" }
            };
            products["5903333333333"] = "D7";

            try
            {
                products.Add("5904444444444", "A3");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("The entry already exists.");
            }

            Console.WriteLine("All products:");
            if (products.Count == 0)
            {
                Console.WriteLine("Empty");
            }
            else
            {
                foreach (KeyValuePair<string, string> product in products)
                {
                    Console.WriteLine($" - {product.Key}: {product.Value}");
                }
            }

            //Buscar un valor
            Console.WriteLine();
            Console.Write("Search by barcode: ");
            string barcode = Console.ReadLine();
            if (products.TryGetValue(barcode, out string location))
            {
                Console.WriteLine($"The product is in the area {location}.");
            }
            else
            {
                Console.WriteLine("The product does not exist.");
            }
        }


        public void detallesUsuario()
        {
            Dictionary<int, Employee> employees = new Dictionary<int, Employee>();
           
            employees.Add(100, new Employee()
            {
                FirstName = "Marcin",
                LastName = "Jamro",
                PhoneNumber = "000-000-000"
            });

            employees.Add(210, new Employee()
            {
                FirstName = "Mary",
                LastName = "Fox",
                PhoneNumber = "111-111-111"
            });

            employees.Add(303, new Employee()
            {
                FirstName = "John",
                LastName = "Smith",
                PhoneNumber = "222-222-222"
            });


            bool isCorrect = true;
            do
            {
                Console.Write("Enter the employee identifier: ");
                string idString = Console.ReadLine();
                isCorrect = int.TryParse(idString, out int id);
                if (isCorrect)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    if (employees.TryGetValue(id, out Employee employee))
                    {
                        Console.WriteLine("First name: {1}{0}Last name:{2}{0} Phone number: {3}", 
                            Environment.NewLine, 
                            employee.FirstName, 
                            employee.LastName, 
                            employee.PhoneNumber);
                    }
                    else
                    {
                        Console.WriteLine("The employee with the given identifier does not exist."); 
                    }
                    Console.ForegroundColor = ConsoleColor.Gray;
                }
            }
            while (isCorrect);
        }
    }
}
