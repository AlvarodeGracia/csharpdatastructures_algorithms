﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpDataStructures_Algorithms._04_Dictionaries_Sets
{
    class SortedSet
    {

        public void ejemplo1_eliminarDuplicados()
        {
            List<string> names = new List<string>()
            {
                "Marcin",
                "Mary",
                "James",
                "Albert",
                "Lily",
                "Emily",
                "marcin",
                "James",
                "Jane"
            };

            SortedSet<string> sorted = new SortedSet<string>(names,
                Comparer<string>.Create((a, b) => a.ToLower().CompareTo(b.ToLower())));

            foreach (string name in sorted)
            {
                Console.WriteLine(name);
            }
        }
    }
}
