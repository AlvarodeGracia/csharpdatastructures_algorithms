﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpDataStructures_Algorithms._04_Dictionaries_Sets
{
    class Hash_Set
    {

        public void ejemplo1_cupones()
        {

            HashSet<int> usedCoupons = new HashSet<int>();
            do
            {
                Console.Write("Enter the coupon number: ");
                string couponString = Console.ReadLine();
                if (int.TryParse(couponString, out int coupon))
                {
                    if (usedCoupons.Contains(coupon))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("It has been already used :-(");
                        Console.ForegroundColor = ConsoleColor.Gray;
                    }
                    else
                    {
                        usedCoupons.Add(coupon);
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Thank you! :-)");
                        Console.ForegroundColor = ConsoleColor.Gray;
                    }
                }
                else
                {
                    break;
                }
            }
            while (true);


            Console.WriteLine();
            Console.WriteLine("A list of used coupons:");
            foreach (int coupon in usedCoupons)
            {
                Console.WriteLine(coupon);
            }
        }


        private static Random random = new Random();
        private static bool GetRandomBoolean()
        {
            return random.Next(2) == 1;
        }

        public void ejemplo2_piscinas()
        {
            Dictionary<PoolTypeEnum, HashSet<int>> tickets = new Dictionary<PoolTypeEnum, HashSet<int>>()
            {
                { PoolTypeEnum.RECREATION, new HashSet<int>() },
                { PoolTypeEnum.COMPETITION, new HashSet<int>() },
                { PoolTypeEnum.THERMAL, new HashSet<int>() },
                { PoolTypeEnum.KIDS, new HashSet<int>() }
            };


            for (int i = 1; i < 100; i++)
            {
                foreach (KeyValuePair<PoolTypeEnum, HashSet<int>> type  in tickets)
                {
                    if (GetRandomBoolean())
                    {
                        type.Value.Add(i);
                    }
                }
            }

            Console.WriteLine("Number of visitors by a pool type:");
            foreach (KeyValuePair<PoolTypeEnum, HashSet<int>> type in tickets)
            {
                Console.WriteLine($" - {type.Key.ToString().ToLower()}: { type.Value.Count}"); 
            }

            PoolTypeEnum maxVisitors = tickets
                .OrderByDescending(t => t.Value.Count)
                .Select(t => t.Key)
                .FirstOrDefault();
            
            Console.WriteLine($"Pool '{maxVisitors.ToString().ToLower()}'  was the most popular.");

            
            //Elementos que han estado en alguna piscina

            HashSet<int> any = new HashSet<int>(tickets[PoolTypeEnum.RECREATION]);

            any.UnionWith(tickets[PoolTypeEnum.COMPETITION]);
            any.UnionWith(tickets[PoolTypeEnum.THERMAL]);
            any.UnionWith(tickets[PoolTypeEnum.KIDS]);

            Console.WriteLine($"{any.Count} people visited at least one pool.");


            //Elementos que estan en todas las listas

            HashSet<int> all =new HashSet<int>(tickets[PoolTypeEnum.RECREATION]);
            all.IntersectWith(tickets[PoolTypeEnum.COMPETITION]);
            all.IntersectWith(tickets[PoolTypeEnum.THERMAL]);
            all.IntersectWith(tickets[PoolTypeEnum.KIDS]);

            Console.WriteLine($"{all.Count} people visited all pools.");
        }
    }
}
