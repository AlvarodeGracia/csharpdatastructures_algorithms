﻿using System;

namespace CSharpDataStructures_Algorithms._01_GettingStarted
{
    class Clases
    {
        public void objetos()
        {
            int age = 28;
            //Se le llama boxing 
            object ageBoxing = age;
            //Se le llama unboxing
            int ageUnboxing = (int)ageBoxing;
        }


        public void usoClases()
        {
            Person person = new Person("Mary", 20);
            person.Relocate("Rzeszow");
            float distance = person.GetDistance("Warsaw");
        }
    }

    //Clases
    public class Person
    {
        private string _location = string.Empty;
        public string Name { get; set; }
        public int Age { get; set; }

        public Person() => Name = "---";

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public void Relocate(string location)
        {
            if (!string.IsNullOrEmpty(location))
            {
                _location = location;
            }
        }

        public float GetDistance(string location)
        {
            return DistanceHelpers.GetDistance(_location, location);
        }
    }

    public class DistanceHelpers
    {
        public static float GetDistance(String location_1, String location_2)
        {
            return 0.1f;
        }
    }

    public interface IDevice
    {
        string Model { get; set; }
        string Number { get; set; }
        int Year { get; set; }

        void Configure(DeviceConfiguration configuration);
        bool Start();
        bool Stop();
    }

    public class DeviceConfiguration
    {
    }
}
