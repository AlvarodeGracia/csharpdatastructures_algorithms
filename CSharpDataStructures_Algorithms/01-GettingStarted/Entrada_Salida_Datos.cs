﻿using System;
using System.Globalization;

namespace CSharpDataStructures_Algorithms._01_GettingStarted
{
    class Entrada_Salida_Datos
    {
        
      
        public void UsoDeEntrada()
        {
            string fullName = Console.ReadLine();

            string numberString = Console.ReadLine();
            bool isCorrecto = int.TryParse(numberString, out int number);

            //Pedir una Fecha
            string dateTimeString = Console.ReadLine();
            if (!DateTime.TryParseExact(
                
                //String original
                dateTimeString,

                //Formato de la fecha
                "M/d/yyyy HH:mm",

                //Cultura adminitda
                new CultureInfo("en-US"),

                //Estilos adicionales
                DateTimeStyles.None,

                //Donde se guarda en la salida
                out DateTime dateTime))
            {
                dateTime = DateTime.Now;
            }


            ConsoleKeyInfo key = Console.ReadKey();
            switch (key.Key)
            {
                case ConsoleKey.S: /* Pressed S */ break;
                case ConsoleKey.F1: /* Pressed F1 */ break;
                case ConsoleKey.Escape: /* Pressed Escape */ break;
            }



           
        }


        public void salida()
        {
            Console.Write("Enter a name: ");
            Console.WriteLine("Hello!");


            string name = "Marcin";
            Console.WriteLine("Hello, {0}!", name);
        } 

        public void ejemplo()
        {
            string tableNumber = "A100";
            int peopleCount = 4;
            DateTime reservationDateTime = new DateTime(
                2017, 10, 28, 11, 0, 0);
            CultureInfo cultureInfo = new CultureInfo("en-US");
            Console.WriteLine(
                "Table {0} has been booked for {1} people on {2} at {3}",
                tableNumber,
                peopleCount,
                reservationDateTime.ToString("M/d/yyyy", cultureInfo),
                reservationDateTime.ToString("HH:mm", cultureInfo));
        }

        public void ejemplo2()
        {
            //Definimos la cultura
            CultureInfo cultureInfo = new CultureInfo("en-US");

            //Escribimos el id de la tabla
            Console.Write("The table number: ");
            string table = Console.ReadLine();

            //Pedimos la cantridad de personas
            Console.Write("The number of people: ");
            string countString = Console.ReadLine();
            int.TryParse(countString, out int count);

            //Fecha d ela reserva
            Console.Write("The reservation date (MM/dd/yyyy): ");
            string dateTimeString = Console.ReadLine();
            if (!DateTime.TryParseExact(
                dateTimeString,
                "M/d/yyyy HH:mm",
                cultureInfo,
                DateTimeStyles.None,
                out DateTime dateTime))
            {
                dateTime = DateTime.Now;
            }

            Console.WriteLine(
                "Table {0} has been booked for {1} people on {2}  at { 3}", 
                table, 
                count, 
                dateTime.ToString("M/d/yyyy", cultureInfo), 
                dateTime.ToString("HH:mm", cultureInfo));
        }
    
       
    }
}
