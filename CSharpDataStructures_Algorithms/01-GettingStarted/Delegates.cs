﻿using System;

namespace CSharpDataStructures_Algorithms._01_GettingStarted
{

    //Es una forma de definir una funcion lambda que puede definir com ofunciona en cuando la necesites y llamarala.
    class Delegates
    {

        delegate double Mean(double a, double b, double c);

        public double Harmonic(double a, double b, double c)
        {
            return 3 / ((1 / a) + (1 / b) + (1 / c));
        }

        public void Ejecutar()
        {
            //Formas de definir un delegate
            Mean arithmetic = (a, b, c) => (a + b + c) / 3;

            Mean geometric = delegate (double a, double b, double c)
            {
                return Math.Pow(a * b * c, 1 / 3.0);
            };

            Mean harmonic = Harmonic;

            //Ejecutar delegates
            double arithmeticResult = arithmetic.Invoke(5, 6.5, 7);
            double geometricResult = geometric.Invoke(5, 6.5, 7);
            double harmonicResult = harmonic.Invoke(5, 6.5, 7);
        }


    }
}
