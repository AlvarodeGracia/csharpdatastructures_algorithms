﻿using System;

namespace CSharpDataStructures_Algorithms._01_GettingStarted
{
    class Tipos
    {
        //Hay dos tipos de valores
        //--tipos por referencia
        //-tipos por valores 
        

        Boolean condicional = false;
        bool condicionalTipo = true;

        Byte byte_1;
        byte byte_2;

        SByte sbyte_1;
        sbyte sbyte_2;

        Int16 short_1;
        short short_2;

        UInt16 ushort_1;
        ushort ushort_2;

        Int32 int_1;
        int int_2;

        UInt32 uint_1;
        uint uint_2;

        Int64 long_1;
        long long_2;

        Char char_1 = 'a';
        char char_2 = 'm';

        Single float_1 = 0.2f;
        float float_2 = 0.3f;

        Double double_1 = 0.4;
        double double_2 = 0.4;

        //Bueno para calculos monetarios
        Decimal decimal_1;
        decimal decimal_2;

        //Es bueno usar enums para evitar los clasicos "PL" en variables
        enum Language { PL, EN, DE };


        public void declaracionIniciacionTipos()
        {
            //Declaracion
            int number;
            //Inicializacion
            number = 500;

            int number2 = 500;

            //Constante
            const int DAYS_IN_WEEK = 7;
        }
        

        public void uso_enum()
        {
            Language language = Language.PL;
            switch (language)
            {
                case Language.PL: /* Polish version */ break;
                case Language.DE: /* German version */ break;
                default: /* English version */ break;
            }
        }

        public void Strings()
        {
            string firstName = "Marcin", lastName = "Jamro";
            int year = 1988;

            //Concatenacion de Strings con otros tipos o otros strings
            //Todo acaba siendo u nString.
            string note = firstName + " " + lastName.ToUpper()
               + " was born in " + year;

            //Acceder a un caracter en concreto
            string initials = firstName[0] + "." + lastName[0] + ".";

            //composite format string
            string note2 = string.Format("{0} {1} was born in {2}", 
                firstName, lastName.ToUpper(), year);

            //interpolated string
            string note3 = $"{firstName} {lastName.ToUpper()} was born in { year} ";


        }

    }

   
}
