﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpDataStructures_Algorithms._06_Graficos
{
    class DFS_BFS
    {

       public  void ejemplo1()
        {
            Graph<int> graph = new Graph<int>(true, true);

            Node<int> n1 = graph.AddNode(1);
            Node<int> n2 = graph.AddNode(2);
            Node<int> n3 = graph.AddNode(3);
            Node<int> n4 = graph.AddNode(4);
            Node<int> n5 = graph.AddNode(5);
            Node<int> n6 = graph.AddNode(6);
            Node<int> n7 = graph.AddNode(7);
            Node<int> n8 = graph.AddNode(8);

            graph.AddEdge(n1, n2, 9);
            graph.AddEdge(n1, n3, 5);
            graph.AddEdge(n2, n1, 3);
            graph.AddEdge(n2, n4, 18);
            graph.AddEdge(n3, n4, 12);
            graph.AddEdge(n4, n2, 2);
            graph.AddEdge(n4, n8, 8);
            graph.AddEdge(n5, n4, 9);
            graph.AddEdge(n5, n6, 2);
            graph.AddEdge(n5, n7, 5);
            graph.AddEdge(n5, n8, 3);
            graph.AddEdge(n6, n7, 1);
            graph.AddEdge(n7, n5, 4);
            graph.AddEdge(n7, n8, 6);
            graph.AddEdge(n8, n5, 3);

            List<Node<int>> dfsNodes = graph.DFS();

            dfsNodes.ForEach(n => Console.WriteLine(n));
        }


        public void ejemplo2()
        {
            Graph<int> graph = new Graph<int>(true, true);

            Node<int> n1 = graph.AddNode(1);
            Node<int> n2 = graph.AddNode(2);
            Node<int> n3 = graph.AddNode(3);
            Node<int> n4 = graph.AddNode(4);
            Node<int> n5 = graph.AddNode(5);
            Node<int> n6 = graph.AddNode(6);
            Node<int> n7 = graph.AddNode(7);
            Node<int> n8 = graph.AddNode(8);

            graph.AddEdge(n1, n2, 9);
            graph.AddEdge(n1, n3, 5);
            graph.AddEdge(n2, n1, 3);
            graph.AddEdge(n2, n4, 18);
            graph.AddEdge(n3, n4, 12);
            graph.AddEdge(n4, n2, 2);
            graph.AddEdge(n4, n8, 8);
            graph.AddEdge(n5, n4, 9);
            graph.AddEdge(n5, n6, 2);
            graph.AddEdge(n5, n7, 5);
            graph.AddEdge(n5, n8, 3);
            graph.AddEdge(n6, n7, 1);
            graph.AddEdge(n7, n5, 4);
            graph.AddEdge(n7, n8, 6);
            graph.AddEdge(n8, n5, 3);

            List<Node<int>> dfsNodes = graph.BFS();

            dfsNodes.ForEach(n => Console.WriteLine(n));
        }

        public void ejemplo3()
        {
            Graph<int> graph = new Graph<int>(true, true);

            Node<int> n1 = graph.AddNode(1);
            Node<int> n2 = graph.AddNode(2);
            Node<int> n3 = graph.AddNode(3);
            Node<int> n4 = graph.AddNode(4);
            Node<int> n5 = graph.AddNode(5);
            Node<int> n6 = graph.AddNode(6);
            Node<int> n7 = graph.AddNode(7);
            Node<int> n8 = graph.AddNode(8);

            graph.AddEdge(n1, n2, 9);
            graph.AddEdge(n1, n3, 5);
            graph.AddEdge(n2, n1, 3);
            graph.AddEdge(n2, n4, 18);
            graph.AddEdge(n3, n4, 12);
            graph.AddEdge(n4, n2, 2);
            graph.AddEdge(n4, n8, 8);
            graph.AddEdge(n5, n4, 9);
            graph.AddEdge(n5, n6, 2);
            graph.AddEdge(n5, n7, 5);
            graph.AddEdge(n5, n8, 3);
            graph.AddEdge(n6, n7, 1);
            graph.AddEdge(n7, n5, 4);
            graph.AddEdge(n7, n8, 6);
            graph.AddEdge(n8, n5, 3);

            List<Edge<int>> mstKruskal = graph.MinimumSpanningTreeKruskal();

            mstKruskal.ForEach(e => Console.WriteLine(e));
        }

        public void ejemplo4()
        {
            Graph<int> graph = new Graph<int>(true, true);

            Node<int> n1 = graph.AddNode(1);
            Node<int> n2 = graph.AddNode(2);
            Node<int> n3 = graph.AddNode(3);
            Node<int> n4 = graph.AddNode(4);
            Node<int> n5 = graph.AddNode(5);
            Node<int> n6 = graph.AddNode(6);
            Node<int> n7 = graph.AddNode(7);
            Node<int> n8 = graph.AddNode(8);

            graph.AddEdge(n1, n2, 9);
            graph.AddEdge(n1, n3, 5);
            graph.AddEdge(n2, n1, 3);
            graph.AddEdge(n2, n4, 18);
            graph.AddEdge(n3, n4, 12);
            graph.AddEdge(n4, n2, 2);
            graph.AddEdge(n4, n8, 8);
            graph.AddEdge(n5, n4, 9);
            graph.AddEdge(n5, n6, 2);
            graph.AddEdge(n5, n7, 5);
            graph.AddEdge(n5, n8, 3);
            graph.AddEdge(n6, n7, 1);
            graph.AddEdge(n7, n5, 4);
            graph.AddEdge(n7, n8, 6);
            graph.AddEdge(n8, n5, 3);

            List<Edge<int>> mstPrim = graph.MinimumSpanningTreePrim();
            mstPrim.ForEach(e => Console.WriteLine(e));
        }

        public void ejemplo5()
        {
            Graph<string> graph = new Graph<string>(false, true);
            
            Node<string> nodeB1 = graph.AddNode("B1");
            Node<string> nodeB2 = graph.AddNode("B2");
            Node<string> nodeB3 = graph.AddNode("B3");
            Node<string> nodeB4 = graph.AddNode("B4");
            Node<string> nodeB5 = graph.AddNode("B5");
            Node<string> nodeB6 = graph.AddNode("B6");

            Node<string> nodeR1 = graph.AddNode("R1");
            Node<string> nodeR2 = graph.AddNode("R2");
            Node<string> nodeR3 = graph.AddNode("R3");
            Node<string> nodeR4 = graph.AddNode("R4");
            Node<string> nodeR5 = graph.AddNode("R5");
            Node<string> nodeR6 = graph.AddNode("R6");


            graph.AddEdge(nodeB1, nodeB3, 20);
            graph.AddEdge(nodeB1, nodeB4, 30);
            graph.AddEdge(nodeB1, nodeB2, 2);
            graph.AddEdge(nodeB2, nodeB3, 30);
            graph.AddEdge(nodeB2, nodeB4, 20);
            graph.AddEdge(nodeB2, nodeR2, 25);
            graph.AddEdge(nodeB3, nodeB4, 2);
            graph.AddEdge(nodeB4, nodeR4, 25);
            graph.AddEdge(nodeB5, nodeR6, 3);
            graph.AddEdge(nodeB5, nodeB6, 6);
            graph.AddEdge(nodeB6, nodeR6, 6);


            graph.AddEdge(nodeR1, nodeR2, 1);
            graph.AddEdge(nodeR1, nodeR5, 75);
            graph.AddEdge(nodeR2, nodeR3, 1);
            graph.AddEdge(nodeR3, nodeR4, 1);
            graph.AddEdge(nodeR3, nodeR6, 100);
            graph.AddEdge(nodeR5, nodeR6, 3);


            Console.WriteLine("Minimum Spanning Tree - Kruskal's Algorithm:");
            List<Edge<string>> mstKruskal =
                graph.MinimumSpanningTreeKruskal();
            mstKruskal.ForEach(e => Console.WriteLine(e));
            Console.WriteLine("Total cost: " + mstKruskal.Sum(e => e.Weight));


            Console.WriteLine("nMinimum Spanning Tree - Prim's Algorithm:");
            List<Edge<string>> mstPrim = graph.MinimumSpanningTreePrim();
            mstPrim.ForEach(e => Console.WriteLine(e));
            Console.WriteLine("Total cost: " + mstPrim.Sum(e => e.Weight));
        }

        public void ejemplo6()
        {
            Graph<int> graph = new Graph<int>(true, true);

            Node<int> n1 = graph.AddNode(1);
            Node<int> n2 = graph.AddNode(2);
            Node<int> n3 = graph.AddNode(3);
            Node<int> n4 = graph.AddNode(4);
            Node<int> n5 = graph.AddNode(5);
            Node<int> n6 = graph.AddNode(6);
            Node<int> n7 = graph.AddNode(7);
            Node<int> n8 = graph.AddNode(8);

            graph.AddEdge(n1, n2, 9);
            graph.AddEdge(n1, n3, 5);
            graph.AddEdge(n2, n1, 3);
            graph.AddEdge(n2, n4, 18);
            graph.AddEdge(n3, n4, 12);
            graph.AddEdge(n4, n2, 2);
            graph.AddEdge(n4, n8, 8);
            graph.AddEdge(n5, n4, 9);
            graph.AddEdge(n5, n6, 2);
            graph.AddEdge(n5, n7, 5);
            graph.AddEdge(n5, n8, 3);
            graph.AddEdge(n6, n7, 1);
            graph.AddEdge(n7, n5, 4);
            graph.AddEdge(n7, n8, 6);
            graph.AddEdge(n8, n5, 3);

            int[] colors = graph.Color();
            for (int i = 0; i < colors.Length; i++)
            {
                Console.WriteLine($"Node {graph.Nodes[i].Data}: {colors[i]}");
            }
        }


        public void ejemplo7()
        {
            Graph<string> graph = new Graph<string>(false, false);

            Node<string> nodePK = graph.AddNode("PK");
            Node<string> nodeLU = graph.AddNode("LU");
            Node<string> nodePD = graph.AddNode("PD");
            Node<string> nodeWM = graph.AddNode("WM");
            Node<string> nodeMZ = graph.AddNode("MZ");
            Node<string> nodeSW = graph.AddNode("SW");
            Node<string> nodeMA = graph.AddNode("MA");
            Node<string> nodeSL = graph.AddNode("SL");
            Node<string> nodeLD = graph.AddNode("LD");
            Node<string> nodeKP = graph.AddNode("KP");
            Node<string> nodePM = graph.AddNode("PM");
            Node<string> nodeZP = graph.AddNode("ZP");
            Node<string> nodeWP = graph.AddNode("WP");
            Node<string> nodeLB = graph.AddNode("LB");
            Node<string> nodeDS = graph.AddNode("DS");
            Node<string> nodeOP = graph.AddNode("OP");

            graph.AddEdge(nodePK, nodeLU);
            graph.AddEdge(nodePK, nodeSW);
            graph.AddEdge(nodePK, nodeMA);
            graph.AddEdge(nodeLU, nodeSW);
            graph.AddEdge(nodeLU, nodeMZ);
            graph.AddEdge(nodeLU, nodePD);
            graph.AddEdge(nodePD, nodeMZ);
            graph.AddEdge(nodePD, nodeWM);
            graph.AddEdge(nodeWM, nodeKP);
            graph.AddEdge(nodeWM, nodePM);
            graph.AddEdge(nodeWM, nodeMZ);
            graph.AddEdge(nodeMZ, nodeKP);
            graph.AddEdge(nodeMZ, nodeLD);
            graph.AddEdge(nodeMZ, nodeSW);
            graph.AddEdge(nodeSW, nodeLD);
            graph.AddEdge(nodeSW, nodeSL);
            graph.AddEdge(nodeSW, nodeMA);
            graph.AddEdge(nodeMA, nodeSL);
            graph.AddEdge(nodeSL, nodeOP);
            graph.AddEdge(nodeSL, nodeLD);
            graph.AddEdge(nodeLD, nodeOP);
            graph.AddEdge(nodeLD, nodeWP);
            graph.AddEdge(nodeLD, nodeKP);
            graph.AddEdge(nodeKP, nodeWP);
            graph.AddEdge(nodeKP, nodePM);
            graph.AddEdge(nodePM, nodeZP);
            graph.AddEdge(nodePM, nodeLB);
            graph.AddEdge(nodePM, nodeWP);
            graph.AddEdge(nodeZP, nodeLB);
            graph.AddEdge(nodeWP, nodeDS);
            graph.AddEdge(nodeWP, nodeOP);
            graph.AddEdge(nodeWP, nodeLB);
            graph.AddEdge(nodeLB, nodeDS);
            graph.AddEdge(nodeDS, nodeOP);

            int[] colors = graph.Color();
            for (int i = 0; i < colors.Length; i++)
            {
                Console.WriteLine($"{graph.Nodes[i].Data}: {colors[i]}");
            }
        }

        public void ejemplo8()
        {
            Graph<int> graph = new Graph<int>(true, true);

            Node<int> n1 = graph.AddNode(1);
            Node<int> n2 = graph.AddNode(2);
            Node<int> n3 = graph.AddNode(3);
            Node<int> n4 = graph.AddNode(4);
            Node<int> n5 = graph.AddNode(5);
            Node<int> n6 = graph.AddNode(6);
            Node<int> n7 = graph.AddNode(7);
            Node<int> n8 = graph.AddNode(8);

            graph.AddEdge(n1, n2, 9);
            graph.AddEdge(n1, n3, 5);
            graph.AddEdge(n2, n1, 3);
            graph.AddEdge(n2, n4, 18);
            graph.AddEdge(n3, n4, 12);
            graph.AddEdge(n4, n2, 2);
            graph.AddEdge(n4, n8, 8);
            graph.AddEdge(n5, n4, 9);
            graph.AddEdge(n5, n6, 2);
            graph.AddEdge(n5, n7, 5);
            graph.AddEdge(n5, n8, 3);
            graph.AddEdge(n6, n7, 1);
            graph.AddEdge(n7, n5, 4);
            graph.AddEdge(n7, n8, 6);
            graph.AddEdge(n8, n5, 3);

            List<Edge<int>> path = graph.GetShortestPathDijkstra(n1, n5);
            path.ForEach(e => Console.WriteLine(e));
        }

        public void mapaJuego()
        {
            string[] lines = new string[]
            {
                "0011100000111110000011111",
                "0011100000111110000011111",
                "0011100000111110000011111",
                "0000000000011100000011111",
                "0000001110000000000011111",
                "0001001110011100000011111",
                "1111111111111110111111100",
                "1111111111111110111111101",
                "1111111111111110111111100",
                "0000000000000000111111110",
                "0000000000000000111111100",
                "0001111111001100000001101",
                "0001111111001100000001100",
                "0001100000000000111111110",
                "1111100000000000111111100",
                "1111100011001100100010001",
                "1111100011001100001000100"
            };
            bool[][] map = new bool[lines.Length][];
            for (int i = 0; i < lines.Length; i++)
            {
                map[i] = lines[i]
                    .Select(c => int.Parse(c.ToString()) == 0)
                    .ToArray();
            }

            Graph<string> graph = new Graph<string>(false, true);
            for (int i = 0; i < map.Length; i++)
            {
                for (int j = 0; j < map[i].Length; j++)
                {
                    if (map[i][j])
                    {
                        Node<string> from = graph.AddNode($"{i}-{j}");

                        if (i > 0 && map[i - 1][j])
                        {
                            Node<string> to = graph.Nodes.Find(
                                n => n.Data == $"{i - 1}-{j}");
                            graph.AddEdge(from, to, 1);
                        }

                        if (j > 0 && map[i][j - 1])
                        {
                            Node<string> to = graph.Nodes.Find(
                                n => n.Data == $"{i}-{j - 1}");
                            graph.AddEdge(from, to, 1);
                        }
                    }
                }
            }

            Node<string> source = graph.Nodes.Find(n => n.Data == "0-0");
            Node<string> target = graph.Nodes.Find(n => n.Data == "16-24");
            List<Edge<string>> path = graph.GetShortestPathDijkstra(
               source, target);


            Console.OutputEncoding = Encoding.UTF8;
            for (int row = 0; row < map.Length; row++)
            {
                for (int column = 0; column < map[row].Length; column++)
                {
                    ConsoleColor color = map[row][column]
                        ? ConsoleColor.Green : ConsoleColor.Red;
                    if (path.Any(e => e.From.Data == $"{row}-{column}"
                        || e.To.Data == $"{row}-{column}"))
                    {
                        color = ConsoleColor.White;
                    }

                    Console.ForegroundColor = color;
                    Console.Write("\u25cf ");
                }
                Console.WriteLine();
            }
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
