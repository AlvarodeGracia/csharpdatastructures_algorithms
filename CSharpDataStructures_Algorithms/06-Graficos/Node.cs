﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpDataStructures_Algorithms._06_Graficos
{
    public class Node<T>
    {
        //almacena un índice de un nodo particular
        public int Index { get; set; }
        //Objeto que representa el nodo
        public T Data { get; set; }
        // lista de adyacencia para un nodo particular
        public List<Node<T>> Neighbors { get; set; } = new List<Node<T>>();
        //almacena los pesos asignados a los bordes adyacentes.
        public List<int> Weights { get; set; } = new List<int>();

        public override string ToString()
        {
            return $"Node with index {Index}: {Data}, neighbors: { Neighbors.Count}"; 
        }
    }
}
