﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpDataStructures_Algorithms._02_Arrays_Lists
{
    class Arrays
    {

        //Tipos
        //-unidimensionales
        //-multidimensionales 
        //-dentadas 

        // la cantidad de elementos en un Array no se puede cambiar después de la inicialización

        public void ArraysUnidimensionales()
        {
            // type[] name;
            int[] numbers;

            numbers = new int[5];

            int[] numbers2 = new int[5];


            numbers[0] = 9;
            numbers[1] = -11; //(...) 
            numbers[4] = 1;


            int[] numbers_3 = new int[] { 9, -11, 6, -12, 1 };

            int[] numbers_4 = { 9, -11, 6, -12, 1 };


            int middle = numbers[2];
        }


        public void nombres_mes()
        {
            //Creamos array con 12 elementos, los 9 meses
            string[] months = new string[12];

            for (int month = 1; month <= 12; month++)
            {
                DateTime firstDay = new DateTime(DateTime.Now.Year, month, 1);
                string name = firstDay.ToString("MMMM",
                    CultureInfo.CreateSpecificCulture("en"));
                months[month - 1] = name;
            }

            foreach (string month in months)
            {
                Console.WriteLine($"-> {month}");
            }
        }

        public void matricesMultidimensionales()
        {
            int[,] numbers = new int[5, 2];
            int[,,] numbers2 = new int[5, 4, 3];

            int[,] numbers_3 = { { 9, 5, -9 }, { -11, 4, 0 }, { 6, 115, 3 }, { -12, -9, 71 }, { 1, -6, -1 } };

            int number = numbers_3[2, 1];
            numbers[1, 0] = 11;
        }


        public void TablaMultiplicar()
        {
            int[,] results = new int[10, 10];

            //results.GetLength(0) numero de elementos en una dimensio nen particular
            for (int i = 0; i < results.GetLength(0); i++)
            {
                for (int j = 0; j < results.GetLength(1); j++)
                {
                    results[i, j] = (i + 1) * (j + 1);
                }
            }


            for (int i = 0; i < results.GetLength(0); i++)
            {
                for (int j = 0; j < results.GetLength(1); j++)
                {
                    Console.Write("{0,4}", results[i, j]);
                }
                Console.WriteLine();
            }
        }

        public void MapaJuego()
        {
            TerrainEnum[,] map =
            {
                {   TerrainEnum.SAND, TerrainEnum.SAND, TerrainEnum.SAND,
                    TerrainEnum.SAND, TerrainEnum.GRASS, TerrainEnum.GRASS,
                    TerrainEnum.GRASS, TerrainEnum.GRASS, TerrainEnum.GRASS,
                    TerrainEnum.GRASS 
                },

                { 
                    TerrainEnum.WATER, TerrainEnum.WATER, TerrainEnum.WATER,
                    TerrainEnum.WATER, TerrainEnum.WATER, TerrainEnum.WATER,
                    TerrainEnum.WATER, TerrainEnum.WALL, TerrainEnum.WATER,
                    TerrainEnum.WATER 
                }
            };

            Console.OutputEncoding = UTF8Encoding.UTF8;
            for (int row = 0; row < map.GetLength(0); row++)
            {
                for (int column = 0; column < map.GetLength(1); column++)
                {
                    Console.ForegroundColor = map[row, column].GetColor();
                    Console.Write(map[row, column].GetChar() + " ");
                }
                Console.WriteLine();
            }
            Console.ForegroundColor = ConsoleColor.Gray;
        }


        public void MatricesDentadas()
        {
            int[][] numbers = new int[4][];
            numbers[0] = new int[] { 9, 5, -9 };
            numbers[1] = new int[] { 0, -3, 12, 51, -3 };
            numbers[3] = new int[] { 54 };

            int[][] numbers_2 =
            {
                new int[] { 9, 5, -9 },
                new int[] { 0, -3, 12, 51, -3 },
                null,
                new int[] { 54 }
            };

            int number = numbers_2[1][2];
            numbers_2[1][3] = 50;



        }

        public void transporte_anual()
        {
            Random random = new Random();
            int transportTypesCount =
                Enum.GetNames(typeof(TransportEnum)).Length;
            TransportEnum[][] transport = new TransportEnum[12][];
            for (int month = 1; month <= 12; month++)
            {
                int daysCount = DateTime.DaysInMonth(
                    DateTime.Now.Year, month);
                transport[month - 1] = new TransportEnum[daysCount];
                for (int day = 1; day <= daysCount; day++)
                {
                    int randomType = random.Next(transportTypesCount);
                    transport[month - 1][day - 1] = (TransportEnum)randomType;
                }
            }

            string[] monthNames = GetMonthNames();
            int monthNamesPart = monthNames.Max(n => n.Length) + 2;
            for (int month = 1; month <= transport.Length; month++)
            {
                Console.Write(
                    $"{monthNames[month - 1]}:".PadRight(monthNamesPart));
                for (int day = 1; day <= transport[month - 1].Length; day++)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor =
                        transport[month - 1][day - 1].GetColor();
                    Console.Write(transport[month - 1][day - 1].GetChar());
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.Write(" ");
                }
                Console.WriteLine();
            }
        }
        private static string[] GetMonthNames()
        {
            string[] names = new string[12];
            for (int month = 1; month <= 12; month++)
            {
                DateTime firstDay = new DateTime(
                    DateTime.Now.Year, month, 1);
                string name = firstDay.ToString("MMMM",
                    CultureInfo.CreateSpecificCulture("en"));
                names[month - 1] = name;
            }
            return names;
        }
    }


}
