﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CSharpDataStructures_Algorithms._02_Arrays_Lists
{
    class Listas
    {

        public void Array_List()
        {
            ArrayList arrayList = new ArrayList();
            arrayList.Add(5);
            arrayList.AddRange(new int[] { 6, -7, 8 });
            arrayList.AddRange(new object[] { "Marcin", "Mary" });
            arrayList.Insert(5, 7.8);

            object first = arrayList[0];
            int third = (int)arrayList[2];


            foreach (object element in arrayList)
            {
                Console.WriteLine(element);
            }

            int count = arrayList.Count;
            int capacity = arrayList.Capacity;


            bool containsMary = arrayList.Contains("Mary");

            int minusIndex = arrayList.IndexOf(-7);

            arrayList.Remove(5);

        }

        public void ValorPromedio()
        {
            List<double> numbers = new List<double>();
            do
            {
                Console.Write("Enter the number: ");
                string numberString = Console.ReadLine();
                if (!double.TryParse(numberString, NumberStyles.Float,
                    new NumberFormatInfo(), out double number))
                {
                    break;
                }

                numbers.Add(number);
                Console.WriteLine($"The average value: {numbers.Average()}");
            } while (true);
        }


        public void ListaPersonas()
        {
            List<Person> people = new List<Person>();
            people.Add(new Person()
            {
                Name = "Marcin",
                Country = CountryEnum.PL,
                Age = 29
            });
            people.Add(new Person()
            {
                Name = "Sabine",
                Country = CountryEnum.DE,
                Age = 25
            }); 


            people.Add(new Person()
            {
                Name = "Ann",
                Country = CountryEnum.PL,
                Age = 31
            });

            List<Person> results = people.OrderBy(p => p.Name).ToList();


            foreach (Person person in results)
            {
                Console.WriteLine($"{person.Name} ({person.Age} years)from { person.Country}."); 
            }


            List<string> names = people.Where(p => p.Age <= 30)
                .OrderBy(p => p.Name)
                .Select(p => p.Name)
                .ToList();
            

        List<string> names_2 = (from p in people
                              where p.Age <= 30
                              orderby p.Name
                              select p.Name).ToList();

        }

        public void Sorted_List()
        {
            SortedList<string, Person> people = new SortedList<string, Person>();

            people.Add("Marcin", new Person()
            {
                Name = "Marcin",
                Country = CountryEnum.PL,
                Age = 29
            });
            people.Add("Sabine", new Person()
            {
                Name = "Sabine",
                Country = CountryEnum.DE,
                Age = 25
            });
            people.Add("Ann", new Person()
            {
                Name = "Ann",
                Country = CountryEnum.PL,
                Age = 31
            });

            foreach (KeyValuePair<string, Person> person in people)
            {
                Console.WriteLine($"{person.Value.Name} ({person.Value.Age} years) from { person.Value.Country}."); 
}

        }

        private static string GetSpaces(int number)
        {
            string result = string.Empty;
            for (int i = 0; i < number; i++)
            {
                result += " ";
            }
            return result;
        }

        public void LustasEnlazadas()
        {
            Page pageFirst = new Page() { Content = "Nowadays (...)" };
            Page pageSecond = new Page() { Content = "Application (...)" };
            Page pageThird = new Page() { Content = "A lot of (...)" };
            Page pageFourth = new Page() { Content = "Do you know (...)" };
            Page pageFifth = new Page() { Content = "While (...)" };
            Page pageSixth = new Page() { Content = "Could you (...)" };

            // **** enlazamos los diferentes componentes
            LinkedList<Page> pages = new LinkedList<Page>();
            pages.AddLast(pageSecond);
            LinkedListNode<Page> nodePageFourth = pages.AddLast(pageFourth);
            pages.AddLast(pageSixth);
            pages.AddFirst(pageFirst);
            pages.AddBefore(nodePageFourth, pageThird);
            pages.AddAfter(nodePageFourth, pageFifth);


            LinkedListNode<Page> current = pages.First;
            int number = 1;
            while (current != null)
            {
                //Limpiamos la consola
                Console.Clear();
                //Numero de la pagina
                string numberString = $"- {number} -";
              
                //Escrivbimos y formateamos el texto
                int leadingSpaces = (90 - numberString.Length) / 2;
                Console.WriteLine(numberString.PadLeft(leadingSpaces
                    + numberString.Length));
                Console.WriteLine();

                string content = current.Value.Content;
                for (int i = 0; i < content.Length; i += 90)
                {
                    string line = content.Substring(i);
                    line = line.Length > 90 ? line.Substring(0, 90) : line;
                    Console.WriteLine(line);
                }

                Console.WriteLine();
                Console.WriteLine($"Quote from Windows Application Development Cookbook by Marcin   Jamro,{ Environment.NewLine}published by Packt Publishing  in 2016."); 
                Console.WriteLine();
                Console.Write(current.Previous != null
                    ? "< PREVIOUS [P]" : GetSpaces(14));
                Console.Write(current.Next != null
                    ? "[N] NEXT >".PadLeft(76) : string.Empty);
                Console.WriteLine();

                //Esperamos a que el usaurio pulse una tecla

                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.N:
                        if (current.Next != null)
                        {
                            current = current.Next; //Sigueinte de la lista
                            number++;
                        }
                        break;
                    case ConsoleKey.P:
                        if (current.Previous != null)
                        {
                            current = current.Previous; //Anterior de la lista
                            number--;
                        }
                        break;
                    default:
                        return;
                }
            }
        }


        public void ListtasEnalazadasCirculares()
        {
            CircularLinkedList<string> categories = new CircularLinkedList<string>();
            categories.AddLast("Sport");
            categories.AddLast("Culture");
            categories.AddLast("History");
            categories.AddLast("Geography");
            categories.AddLast("People");
            categories.AddLast("Technology");
            categories.AddLast("Nature");
            categories.AddLast("Science");

            Random random = new Random();
            int totalTime = 0;
            int remainingTime = 0;

            foreach (string category in categories)
            {
                if (remainingTime <= 0)
                {
                    Console.WriteLine("Press [Enter] to start  or any other to exit."); 
                    switch (Console.ReadKey().Key)
                    {
                        case ConsoleKey.Enter:
                            totalTime = random.Next(1000, 5000);
                            remainingTime = totalTime;
                            break;
                        default:
                            return;
                    }
                }

                int categoryTime = (-450 * remainingTime) / (totalTime - 50)
                    + 500 + (22500 / (totalTime - 50));
                remainingTime -= categoryTime;
                Thread.Sleep(categoryTime);

                Console.ForegroundColor = remainingTime <= 0
                    ? ConsoleColor.Red : ConsoleColor.Gray;
                Console.WriteLine(category);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }
    }





}
