﻿using System;
using System.Collections.Generic;
using System.DataStructures;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpDataStructures_Algorithms._05_Arboles.Arboles_AVL
{
    //Este tipo de arboles son arboles equilibrados, segun se van modificando mantiene la altura repartiendo sus nodos entre las rammas
    //ASi no se queda un arbol flaco con muchas alturas.

    //Para esto usaremos el nugget Adjunct -> adjunct-System.DataStructures.AvlTree
    class ArbolesAVL
    {

        public void ejemplo1_mantenerEquilibrio()
        {
            AvlTree<int> tree = new AvlTree<int>();
            for (int i = 1; i < 10; i++)
            {
                tree.Add(i);
            }

            //GetInorderEnumerator
            Console.WriteLine("In-order: " + string.Join(", ", tree.GetInorderEnumerator()));
            //GetPostorderEnumerator
            Console.WriteLine("Post-order: "+ string.Join(", ", tree.GetPostorderEnumerator()));
            //GetBreadthFirstEnumerator
            Console.WriteLine("Breadth-first: "+ string.Join(", ", tree.GetBreadthFirstEnumerator()));

            //Buscamos el nodo 8
            AvlTreeNode<int> node = tree.FindNode(8);
            
            //Sacamos los datos de ese nodo
            Console.WriteLine($"Children of node {node.Value} (height =  { node.Height}): { node.Left.Value}and { node.Right.Value}."); 


        }

    }
}
