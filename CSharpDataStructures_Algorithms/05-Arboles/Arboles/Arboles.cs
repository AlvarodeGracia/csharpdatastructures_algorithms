﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpDataStructures_Algorithms._05_Arboles.Arboles
{
    class Arboles
    {

        public void ejemplo1_jerarquía_identificadores()
        {
            Tree<int> tree = new Tree<int>();

            tree.Root = new TreeNode<int>() { Data = 100 };

            //Agregamos 3 hijos al padre
            tree.Root.Children = new List<TreeNode<int>>
            {
                new TreeNode<int>() { Data = 50, Parent = tree.Root },
                new TreeNode<int>() { Data = 1, Parent = tree.Root },
                new TreeNode<int>() { Data = 150, Parent = tree.Root }
            };

            //Agreamos un hijo al al hijo 3
            tree.Root.Children[2].Children = new List<TreeNode<int>>()
            {
                new TreeNode<int>()
                    { Data = 30, Parent = tree.Root.Children[2] }
            };


        }

        public void ejemplo2_estructura_empresa()
        {

            Tree<Person> company = new Tree<Person>();
            company.Root = new TreeNode<Person>()
            {
                Data = new Person(100, "Marcin Jamro", "CEO"),
                Parent = null
            };


            company.Root.Children = new List<TreeNode<Person>>()
            {
                new TreeNode<Person>()
                {
                    Data = new Person(1, "John Smith", "Head of Development"),
                    Parent = company.Root
                },
                new TreeNode<Person>()
                {
                    Data = new Person(50, "Mary Fox", "Head of Research"),
                    Parent = company.Root
                },
                new TreeNode<Person>()
                {
                    Data = new Person(150, "Lily Smith", "Head of Sales"),
                    Parent = company.Root
                }
            };
            
            company.Root.Children[2].Children = new List<TreeNode<Person>>()
            {
                new TreeNode<Person>()
                {
                    Data = new Person(30, "Anthony Black", "Sales Specialist"),Parent = company.Root.Children[2]
                }
            };

        }
    }
}
