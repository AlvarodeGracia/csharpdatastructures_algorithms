﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreeLib;

namespace CSharpDataStructures_Algorithms._05_Arboles.ArbolesRojoNegro
{
    //-Cada nodo debe ser de color rojo o negro. Por lo tanto, debe agregar datos adicionales para un nodo que almacena un color.
    //-Todos los nodos con valores no pueden ser nodos hoja. Por esta razón, los pseudo-nodos NIL deben usarse como hojas en el árbol, 
    //      mientras que todos los demás nodos son internos. Además, todos los pseudo-nodos NIL deben ser negros.
    //-Si un nodo es rojo, sus dos hijos deben ser negros.
    //-Para cualquier nodo, el número de nodos negros en la ruta a una hoja descendente (es decir, el pseudo-nodo NIL) debe ser el mismo.

    //Necesitamos la libreria TreeLib 

    //Es tan eficiente como Arboles AVL ademas de que en el peor de los casos se mantiene la eficiencia por l oque es mejor.

    class ArbolesRojoNegro
    {

        public void ejemplo1_usodeRBT()
        {
            //Instanmciamos un Arbol RBT para gaurdar los numeros del 1 al 10
            RedBlackTreeList<int> tree = new RedBlackTreeList<int>();
            for (int i = 1; i <= 10; i++)
            {
                tree.Add(i);
            }

            //Eliminamos el nodo con valor 9
            tree.Remove(9);

            //Preguntamos si existe un nodo con valor 5
            bool contains = tree.ContainsKey(5);
            Console.WriteLine(
                "Does value exist? " + (contains ? "yes" : "no"));

            //Datos sobre el tamaño y el valor mayor y menor
            uint count = tree.Count;
            tree.Greatest(out int greatest);
            tree.Least(out int least);
            Console.WriteLine(
                $"{count} elements in the range {least}-{greatest}");

            //Obtener un enumerable de los nodos
            Console.WriteLine( "Values: " + string.Join(", ", tree.GetEnumerable()));

            //Otra forma d eobtener los valores con u nbucle
            Console.Write("Values: ");
            foreach (EntryList<int> node in tree)
            {
                Console.Write(node + " ");
            }



        }
    }
}
