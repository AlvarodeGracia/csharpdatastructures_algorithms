﻿using DIBRIS.Hippie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpDataStructures_Algorithms._05_Arboles.Montones
{
    //-Para min-heap : el valor de cada nodo debe ser mayor o igual que el valor de su nodo padre
    //-Para max-heap : el valor de cada nodo debe ser menor o igual que el valor de su nodo principal


    //Usamos el Nugget Hippie 
    class Montones
    {
        //el algoritmo de ordenación del montón tiene  O (n * log (n)) complejidad de tiempo.
        public void ejemplo1_ordenandoMontonBinario()
        {
            //Tenemos una lista de valores
            List<int> unsorted = new List<int>() { 50, 33, 78, -23, 90, 41 };
            MultiHeap<int> heap = HeapFactory.NewBinaryHeap<int>();
            //Los vamosa gregando
            unsorted.ForEach(i => heap.Add(i));
            //los mostramos desordenados
            Console.WriteLine("Unsorted: " + string.Join(", ", unsorted));

            //Creamos una lista donde vamos a mostrarlos ordenados
            List<int> sorted = new List<int>(heap.Count);
            //Vamos obteniendo el valor maws bajo
            while (heap.Count > 0)
            {
                sorted.Add(heap.RemoveMin());
            }


            Console.WriteLine("Sorted: " + string.Join(", ", sorted));
        }


        public void ejemplo1_ordenandoMontonBinomiales()
        {
            //Tenemos una lista de valores
            List<int> unsorted = new List<int>() { 50, 33, 78, -23, 90, 41 };
            MultiHeap<int> heap = HeapFactory.NewBinomialHeap<int>();
            //Los vamosa gregando
            unsorted.ForEach(i => heap.Add(i));
            //los mostramos desordenados
            Console.WriteLine("Unsorted: " + string.Join(", ", unsorted));

            //Creamos una lista donde vamos a mostrarlos ordenados
            List<int> sorted = new List<int>(heap.Count);
            //Vamos obteniendo el valor maws bajo
            while (heap.Count > 0)
            {
                sorted.Add(heap.RemoveMin());
            }


            Console.WriteLine("Sorted: " + string.Join(", ", sorted));
        }


        public void ejemplo1_ordenandoMontonFibonacci()
        {
            //Tenemos una lista de valores
            List<int> unsorted = new List<int>() { 50, 33, 78, -23, 90, 41 };
            MultiHeap<int> heap = HeapFactory.NewFibonacciHeap<int>();
            //Los vamosa gregando
            unsorted.ForEach(i => heap.Add(i));
            //los mostramos desordenados
            Console.WriteLine("Unsorted: " + string.Join(", ", unsorted));

            //Creamos una lista donde vamos a mostrarlos ordenados
            List<int> sorted = new List<int>(heap.Count);
            //Vamos obteniendo el valor maws bajo
            while (heap.Count > 0)
            {
                sorted.Add(heap.RemoveMin());
            }


            Console.WriteLine("Sorted: " + string.Join(", ", sorted));
        }
    }
}
