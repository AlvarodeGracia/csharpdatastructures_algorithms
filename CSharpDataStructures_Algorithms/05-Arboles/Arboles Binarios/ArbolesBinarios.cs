﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpDataStructures_Algorithms._05_Arboles.ArbolesBinarios
{
    class ArbolesBinarios
    {

        public void ejemplo1_cuestionario()
        {
            BinaryTree<QuizItem> tree = GetTree();
            BinaryTreeNode<QuizItem> node = tree.Root;
            while (node != null)
            {
                if (node.Left != null || node.Right != null)
                {
                    Console.Write(node.Data.Text);
                    switch (Console.ReadKey(true).Key)
                    {
                        case ConsoleKey.Y:
                            WriteAnswer(" Yes");
                            node = node.Left;
                            break;
                        case ConsoleKey.N:
                            WriteAnswer(" No");
                            node = node.Right;
                            break;
                    }
                }
                else
                {
                    WriteAnswer(node.Data.Text);
                    node = null;
                }
            }
        }

        private static BinaryTree<QuizItem> GetTree()
        {
            BinaryTree<QuizItem> tree = new BinaryTree<QuizItem>();

            tree.Root = new BinaryTreeNode<QuizItem>()
            {
                Data = new QuizItem("Do you have experience in developing applications ? "), 
                Children = new List<TreeNode<QuizItem>>()
                {
                    new BinaryTreeNode<QuizItem>()
                    {
                        Data = new QuizItem("Have you worked as a developer for more than 5 years ? "), 
                        
                        Children = new List<TreeNode<QuizItem>>()
                        {
                            new BinaryTreeNode<QuizItem>()
                            {
                                Data = new QuizItem("Apply as a senior  developer!") 
                            },
                            new BinaryTreeNode<QuizItem>()
                            {
                                Data = new QuizItem("Apply as a middle developer!") 
                            }
                        }
                    }, 
                    new BinaryTreeNode<QuizItem>() 
                    { 
                        Data = new QuizItem("Have you completed the university?"), 
                        Children = new List<TreeNode<QuizItem>>()
                        {
                            new BinaryTreeNode<QuizItem>()
                            {
                                Data = new QuizItem("Apply for a junior developer!") 
                            },
                            new BinaryTreeNode<QuizItem>()
                            {
                                Data = new QuizItem("Will you find some time during the semester?"), 
                                Children = new List<TreeNode<QuizItem>>()
                                {
                                    new BinaryTreeNode<QuizItem>()
                                    {
                                        Data = new QuizItem("Apply for our long-time internship program!") 
                                    },
                                    new BinaryTreeNode<QuizItem>()
                                    {
                                        Data = new QuizItem("Apply for summer internship program!") 
                                    }
                                }
                            }
                        }
                    }
                } 
            }; 
            tree.Count = 9; 
            return tree; 
        }

        private static void WriteAnswer(string text)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        private const int COLUMN_WIDTH = 5;
        public void ejemplo2_visualización_BST()
        {
            
                Console.OutputEncoding = Encoding.UTF8;

                BinarySearchTree<int> tree = new BinarySearchTree<int>();
                tree.Root = new BinaryTreeNode<int>() { Data = 100 };
                tree.Root.Left = new BinaryTreeNode<int>()
                { Data = 50, Parent = tree.Root };
                tree.Root.Right = new BinaryTreeNode<int>()
                { Data = 150, Parent = tree.Root };
                tree.Count = 3;
                VisualizeTree(tree, "The BST with three nodes (50, 100, 150):"); 

                tree.Add(75);
                tree.Add(125);
                VisualizeTree(tree, "The BST after adding two nodes  (75, 125):");
    

                tree.Remove(25);

                VisualizeTree(tree,"The BST after removing the node 25:"); 
 


            Console.Write("Pre-order traversal:\t");
            Console.Write(string.Join(", ", tree.Traverse( TraversalEnum.PREORDER).Select(n => n.Data)));
            Console.Write("\nIn-order traversal:\t");
            Console.Write(string.Join(", ", tree.Traverse( TraversalEnum.INORDER).Select(n => n.Data)));
            Console.Write("\nPost-order traversal:\t");
            Console.Write(string.Join(", ", tree.Traverse(TraversalEnum.POSTORDER).Select(n => n.Data)));
        


    }
        private static void VisualizeTree(BinarySearchTree<int> tree, string caption)
        {
            char[][] console = InitializeVisualization(
                tree, out int width);
            VisualizeNode(tree.Root, 0, width / 2, console, width);
            Console.WriteLine(caption);
            foreach (char[] row in console)
            {
                Console.WriteLine(row);
            }
        }

        private static char[][] InitializeVisualization(BinarySearchTree<int> tree, out int width)
        {
            int height = tree.GetHeight();
            width = (int)Math.Pow(2, height) - 1;
            char[][] console = new char[height * 2][];
            for (int i = 0; i < height * 2; i++)
            {
                console[i] = new char[COLUMN_WIDTH * width];
            }
            return console;
        }

        private static void VisualizeNode(BinaryTreeNode<int> node, int row, int column, char[][] console, int width)
        {
            if (node != null)
            {
                char[] chars = node.Data.ToString().ToCharArray();
                int margin = (COLUMN_WIDTH - chars.Length) / 2;
                for (int i = 0; i < chars.Length; i++)
                {
                    console[row][COLUMN_WIDTH * column + i + margin]
                        = chars[i];
                }

                int columnDelta = (width + 1) /
                    (int)Math.Pow(2, node.GetHeight() + 1);
                VisualizeNode(node.Left, row + 2, column - columnDelta,
                    console, width);
                VisualizeNode(node.Right, row + 2, column + columnDelta,
                    console, width);
                DrawLineLeft(node, row, column, console, columnDelta);
                DrawLineRight(node, row, column, console, columnDelta);
            }
        }

        private static void DrawLineLeft(BinaryTreeNode<int> node,
    int row, int column, char[][] console, int columnDelta)
        {
            if (node.Left != null)
            {
                int startColumnIndex =
                    COLUMN_WIDTH * (column - columnDelta) + 2;
                int endColumnIndex = COLUMN_WIDTH * column + 2;
                for (int x = startColumnIndex + 1;
                    x < endColumnIndex; x++)
                {
                    console[row + 1][x] = '-';
                }
                console[row + 1][startColumnIndex] = '\u250c';
                console[row + 1][endColumnIndex] = '+';
            }
        }

        private static void DrawLineRight(BinaryTreeNode<int> node,
    int row, int column, char[][] console, int columnDelta)
        {
            if (node.Right != null)
            {
                int startColumnIndex = COLUMN_WIDTH * column + 2;
                int endColumnIndex =
                    COLUMN_WIDTH * (column + columnDelta) + 2;
                for (int x = startColumnIndex + 1;
                    x < endColumnIndex; x++)
                {
                    console[row + 1][x] = '-';
                }
                console[row + 1][startColumnIndex] = '+';
                console[row + 1][endColumnIndex] = '\u2510';
            }
        }
    }

}
