﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpDataStructures_Algorithms._05_Arboles.ArbolesBinarios
{
    public enum TraversalEnum
    {
        PREORDER,
        INORDER,
        POSTORDER
    }
}
